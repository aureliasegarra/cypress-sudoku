import React from 'react';
import { Numbers } from './Numbers';
import '../App.css';
import { SudokuContext } from '../context/SudokuContext';

describe('Numbers', { viewportHeight: 1000, viewportWidth: 1000 },  () => {
    it('should shows numbers',  () => {
        cy.mount(<Numbers/>);
        cy.get('.status__number').should('have.length', 9);
    });

    it('should calls on click number',  () => {
        cy.mount(
            <Numbers onClickNumber={cy.stub().as('click')}/>
        );
        cy.get('.status__number').should('have.length', 9);
        cy.contains('.status__number', '1').click();
        cy.get('@click').should('have.been.calledOnceWith', '1');
    });

    it.only('should show the selected number',  () => {
        cy.mount(
            <SudokuContext.Provider value={{ numberSelected: '8'}}>
                <Numbers onClickNumber={cy.stub().as('click')}/>
            </SudokuContext.Provider>
        );
        cy.contains('.status__number', '8')
            .should('have.class', 'status__number--selected');

    });
});