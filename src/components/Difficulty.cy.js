import React from 'react';
import { Difficulty } from './Difficulty';
import '../App.css';
import {SudokuContext, SudokuProvider} from '../context/SudokuContext';

describe('Difficulty',  () => {
    it('should change the difficulty level',  () => {
        cy.mount(
            <SudokuProvider value={{ difficulty: 'easy' }}>
                <div className="innercontainer">
                    <section className="status">
                        <Difficulty onChange={cy.stub().as('change')} />
                    </section>
                </div>
            </SudokuProvider>
        )
        cy.get('select').should('have.value', 'Easy').select('Medium');
        cy.get('select').should('have.value', 'Medium').select('Hard');
        cy.get('@change')
            .should('have.been.calledTwice')
            .its('firstCall.args.0.target.value')
            .then(cy.log)
            //.should('equal', 'Easy');

    });
});