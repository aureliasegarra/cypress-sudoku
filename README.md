[![Front](https://img.shields.io/badge/Test-Cypress-green?style=flat)](https://)
[![Front](https://img.shields.io/badge/Context-E2ETesting-blue?style=flat)](https://)
[![Front](https://img.shields.io/badge/Context-ComponentTesting-pink?style=flat)](https://)



<br/>
<div align="center">
    <img src="images/settings.png" alt="Logo" width="15%">
    <br/>
    <br/>
    <h1 align="center">Cypress</h1>
    <h3>E2E & Component testing</h3>
</div>
  <p align="center">
    React Sudoku
    <br />
    <br />
  </p>


<br/>
<br/>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>

  <ol>
    <li><a href="#illustration">Illustration</a></li>
    <li><a href="#description">Description</a></li>
    <li><a href="#languages">Languages & tools</a></li>
    <li><a href="#objectives">Objectives</a></li>
    <li><a href="#setup">SetUp</a></li>
    <li><a href="#status">Status</a></li>
    <li><a href="#context">Context</a></li>
  </ol>
</details>

<br>
<br>



## ✨ Illustration <a id="illustration"></a>
![illustration](images/e2e_screenshot.png)


## 🗒 Description <a id="description"></a>
Practical project to improve Cypress version 10 with its enhanced end-to-end 
and component testing features by testing a React Sudoku built by Amith Raravi
https://github.com/raravi/sudoku
- how to set up Cypress and debug web apps
- features such as controlling the page clock, network stubbing, and screenshots of test failures, which make finding defects and debugging apps quick and simple



## 🛠 Languages/tools <a id="languages"></a>
- Javascript
- Mocha
- Cypress v.10.11


## 🎯 Objectives <a id="objectives"></a>
- Improve Cypress configuration
- Improve E2E testing & Component testing
- Run Cypress on CI services
- Extend Cypress with plugins

## 🎯 SetUp <a id="setup"></a>
- How to launch React Sudoku
```
  npm run start
```
- How to launch E2E testing
```
  npm run cy:e2e
```
- How to launch React Sudoku AND Cypress E2E testing in Chrome browser
```
  npm run e2e
```

## 📈 Status <a id="status"></a>

Project in progress ... 


## 🗓 Context <a id="context"> </a>
I realized this practical work during my bachelor of computer science in the Institute of technology of Vannes

