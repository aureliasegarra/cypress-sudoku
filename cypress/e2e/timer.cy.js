describe('Timer',  () => {
    it('shows 10 seconds', () => {
        cy.visit('/');
        for(let i = 0; i < 10; i++) {
            cy.contains('.status__time', `00:0${i}`);
        }
    });

    it('shows minutes and secondes since the game started',  () => {
        cy.clock();
        cy.visit('/');
        cy.get('.status__time').contains('00:00');
        // confirm the timer shows '00:30' after 30 seconds
        cy.tick(30_000);
        cy.contains('.status__time', '00:30');
        // confirm the timer shows '1:00' after 60 seconds
        cy.tick(30_000);
        cy.contains('.status__time', '01:00');
        // confirm the timer shows '11:40' after 700 seconds
        cy.tick(640_000);
        cy.contains('.status__time', '11:40');


    });
});